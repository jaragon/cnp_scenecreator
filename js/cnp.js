var drag = true;
var rotate = false;
var scale = 1;
var df_w = 140;
var df_h = 140;
var remove = 0;
var Plaintiff = 0;
var Defendant = 0;
var Neutral = 0;
var setColor = 0;
var fromReset = 0;

function spaceBackground(d) {
    result = d.match(/uploads\/(.*)"\s/img);
    var bkg = result[0];
    background = bkg.replace(/"/img, "");
    $('#space').css('background', 'url(' + background + ')');
}

function toggle_upload() {
    $('#upload').toggle('fast');
}

function toggle_signage() {
    $('#signage').toggle('fast');
}

function toggle_color() {
    $('#color').toggle('fast');
    if (setColor == 0) {
        setColor = 1;
        $('#notice').html("COLOR MODE ON");
    } else {
        setColor = 0;
        $('#notice').html("COLOR MODE OFF");
    }
}

function insertImage(image, w, h, ox, oy, ly) {
    var item = new Image();
    item.src = $('#_' + image).attr('src');
    drawImage(item, w, h, ox, oy, ly);
}

function insertRoad(image, w, h, ox, oy, ly) {
    var item = new Image();
    item.src = $('#_' + image).attr('src');
    drawRoad(item, w, h, ox, oy, ly);
}

function insertObstacles(image, w, h, ox, oy, ly) {
    var item = new Image();
    item.src = $('#_' + image).attr('src');
    drawObstacles(item, w, h, ox, oy, ly);
}

$('#create_label').live('click', function () {
    createLabel($('#label_text').val(), $('#label_direction').val());
    $('#signage').toggle('fast');
    return false;
});

$('#save').live('click', function () {
    var json = stage.toJSON();
    $('#savespace').val(json);
});

$("li[title='Plaintiff']").live('click', function () {
    Plaintiff = 1;
    Defendant = 0;
    Neutral = 0;
});

$("li[title='Defendant']").live('click', function () {
    Defendant = 1;
    Plaintiff = 0;
    Neutral = 0;
});

$("li[title='Neutral']").live('click', function () {
    Neutral = 1;
    Plaintiff = 0;
    Defendant = 0;
});

function rotate_on() {
    if (rotate) {
        drag = true;
        rotate = false;
        $('#rotate_image').src = 'images/icon-rotatebw.png';
        $('#notice').html("DRAG MODE ON");
    } else {
        drag = false;
        rotate = true;
        if (scale != 1) { 
            stage.get('.storage').each(function(i){ i.attrs.x = 1 })
        }
        scale=1
        layer.setScale(scale);
        layer.draw();        
        $('#rotate_image').src = 'images/icon-rotate.png';
        $('#notice').html("ROTATE MODE ON");
    }
}

function export_image() {
    stage.toDataURL({
        width: 804,
        height: 614,
        mimeType: "image/png",
        callback: function (dataUrl) {
            window.open(dataUrl);
        }
    });
}

function set_scale(dir) {
    if (dir == 'up') {  
        scale += 0.1;  
        drag = true;
        rotate = false;
        $('#rotate_image').src = 'images/icon-rotatebw.png';
        $('#notice').html("DRAG MODE ON");
    }        
    if (dir == 'dn') { 
        scale -= 0.1;
        drag = true;
        rotate = false;
        $('#rotate_image').src = 'images/icon-rotatebw.png';
        $('#notice').html("DRAG MODE ON");
        
    }
    if (dir == '0') scale = 1;
    var statusline = $('#notice').html();
    if (statusline.match(/ZOOM/im)) {
        result = statusline.split(/ZOOM/im);
        $('#notice').html( result[0] + " ZOOM " + scale.toFixed(2))
    } else {
        $('#notice').html( statusline + " ZOOM " + scale.toFixed(2))  
    }
    layer.setScale(scale);
    layer.draw();
}

$('#save').live('click', function () {
    var json = stage.toJSON();
    $('#savespace').val(json);
});

function set_delete() {
    if (remove == 0) {
        $('#del_image').src = 'images/icon-rotatebw.png';
        $('#notice').html("DELETE MODE");
        remove = 1;
    } else {
        $('#del_image').src = 'images/icon-rotate.png';
        $('#notice').html("EDIT MODE");
        remove = 0;
    }
}
