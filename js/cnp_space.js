    function radians(degrees) {
        return degrees * (Math.PI / 180)
    }

    function degrees(radians) {
        return radians * (180 / Math.PI)
    }

    function angle(cx, cy, px, py) {
        var x = cx - px;
        var y = cy - py;
        return Math.atan2(-y, -x)
    }

    function distance(p1x, p1y, p2x, p2y) {
        return Math.sqrt(Math.pow((p2x - p1x), 2) + Math.pow((p2y - p1y), 2))
    }
    var stage = new Kinetic.Stage({
        container: 'space',
        width: 804,
        height: 614,
        draggable: false
    })
    var layer = new Kinetic.Layer();
    stage.add(layer)
    var px = 0;
    var py = 0;
    var rotate = 0;
    var isDragging = 1;
    var refRotation = null;

    function drawImage(imageObj, d_w, d_h, ox, oy, ly) {
        var group = new Kinetic.Group({
            x: 402,
            y: 307
        });
        layer.add(group);
        ofy = parseInt(d_w / 2);
        ofx = parseInt(d_h / 2);
        var cnpImage = new Kinetic.Image({
            image: imageObj,
            x: 0,
            y: 0,
            width: d_w,
            height: d_h,
            offset: [ofx, ofy],
            dragOnTop: true,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
            shadowOpacity: 0.5,
            shadowEnabled: false,
            snap: false,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                var groupPos = group.getPosition()
                var rotation = degrees(angle(groupPos.x, groupPos.y, pos.x, pos.y))
                if (rotate) {
                    isDragging = false;
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    isDragging = true;
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });
        group.add(cnpImage)


        cnpImage.on('mouseover', function () {
            document.body.style.cursor = 'pointer';
            this.setStrokeWidth(2);
            //COVER.attrs.opacity = 100;
            //this.dashArray: [33, 10]
            //console.info('car rotation ' + this.rotationDeg)
            //console.info(this)
            //this.setRotationDeg(20); MOVE just the car
            //group.setRotationDeg(90); MOVE everything but control
            //console.warn(group.children[1].attrs.x, group.children[1].attrs.y);
            //console.warn(controlGroup)
            //console.warn(cnpImage)
            //console.warn(' x y ' + cnpImage.getAbsolutePosition().x + ' ' + cnpImage.getAbsolutePosition().y)
            //controlGroup.setPosition(400,500)
            //calcSignOpacity()
            layer.draw();
        });
        cnpImage.on('mouseout', function () {
            document.body.style.cursor = 'default';
            this.setStrokeWidth(0);
            //COVER.attrs.opacity = 0;
            //calcSignOpacity()
            layer.draw();
        });
        cnpImage.on('click', function () {
            if (remove == 1) {
                this.remove()
                control.remove()
                controlHOME.remove()
                line.remove()
                COVER.remove()
                controlGroup.remove()
            }
            if (setColor == 1) {
                if (Plaintiff) {
                    imageObj.src = imageObj.src.replace(/-green/img, "");
                    imageObj.src = imageObj.src.replace(/-red/img, "");
                    imageObj.src = imageObj.src.replace(/-blue/img, "");
                    imageObj.src = imageObj.src.replace(/\.png/img, "-green$&");
                    control.setFill('green')
                }
                if (Defendant) {
                    imageObj.src = imageObj.src.replace(/-green/img, "");
                    imageObj.src = imageObj.src.replace(/-red/img, "");
                    imageObj.src = imageObj.src.replace(/-blue/img, "");
                    imageObj.src = imageObj.src.replace(/\.png/img, "-red$&");
                    control.setFill('red')
                }
                if (Neutral) {
                    imageObj.src = imageObj.src.replace(/-green/img, "");
                    imageObj.src = imageObj.src.replace(/-red/img, "");
                    imageObj.src = imageObj.src.replace(/-blue/img, "");
                    imageObj.src = imageObj.src.replace(/\.png/img, "-blue$&");
                    control.setFill('blue')
                }
            }
            layer.draw();
        });
        cnpImage.on('dragstart', function () {

        });
        cnpImage.on('dragend', function () {
            if (!rotate) {
                isDragging = true;
                if (scale == 1) {
                    px = cnpImage.getAbsolutePosition().x
                    py = cnpImage.getAbsolutePosition().y
                    group.setPosition(px, py)
                    cnpImage.setPosition(0, 0)
                    controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
                }
                calcSignOpacity()
            } else {
                //controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
                isDragging = false;
                calcSignOpacity()
            }
            layer.draw();
        });
        var controlGroup = new Kinetic.Group({
            x: group.getPosition().x + cnpImage.getWidth() * 1 + 10,
            y: group.getPosition().y,
            opacity: 0,
            draggable: true
        });
        layer.add(controlGroup)
        controlGroup.x = group.getPosition().x + cnpImage.getWidth() * 1 + 10;
        //controlGroup.y = group.getPosition().y + cnpImage.getHeight() * cnpImage.getScale().y + 10; // added
        // shitscale
        // 
        var store = new Kinetic.Circle({
            x: 1,
            y: 0,
            fill: 'green',
            opacity: 0,
            radius: 2,
            name: 'storage'
        }); 
        controlGroup.add(store)       


        var control = new Kinetic.Circle({
            x: 0,
            y: 0,
            fill: 'yellow',
            opacity: 0,
            radius: 17
        });
        controlGroup.add(control)

        // handleImage = new Image();
        // handleImage.src = '/images/handle.png';
        // var control = new Kinetic.Image({
        //     x: 0, 
        //     y: 0,
        //     image: handleImage, 
        //     width: 34,
        //     height: 34,
        //     draggable: true,
        //     dragOnTop: true
        // })

        var controlHOME = new Kinetic.Circle({
            x: 0,
            y: 0,
            fill: 'yellow',
            opacity: 0,
            radius: 17,
            name: 'control'
        });


        group.add(controlHOME)
        controlHOME.on('mouseover', function () {
            // console.warn('controlHOME ' +this.getAbsolutePosition().x + ' ' + this.getAbsolutePosition().y)
            // console.warn('controlHOME ' +controlHOME.getAbsolutePosition().x + ' ' + controlHOME.getAbsolutePosition().y)
            controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
        });
        controlGroup.on('click', function () {
            // console.warn('controlGroup ' +this.getAbsolutePosition().x + ' ' + this.getAbsolutePosition().y)
            // console.warn('controlHOME ' +controlHOME.getAbsolutePosition().x + ' ' + controlHOME.getAbsolutePosition().y)
            // controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
        });
        var COVER = new Kinetic.Circle({
            x: 0,
            y: 0,
            stroke: 'black',
            opacity: 100,
            dashArray: [10, 4],
            draggable: false,
            visible: false,
            radius: cnpImage.getWidth() /2
        });
        group.add(COVER)
        COVER.moveDown();

        controlGroup.on('mouseover', function () {
            COVER.attrs.visible=true;
        });

        controlGroup.on('mouseout', function () {
            COVER.attrs.visible=false;
        });

        var line = new Kinetic.Line({
            points: linePoints(0),
            stroke: 'black',
            opacity: 50,
        });
        group.add(line)

        function linePoints(dis) {
            //stage.get('.control').each(function(i){ console.warn(i.attrs.opacity=100) })
            return [
                [(cnpImage.getWidth() / 2) * 1, group.getOffset().y], 
                [(cnpImage.getWidth() / 2) * 1 + Math.max(dis, 0), group.getOffset().y]
            ]
        }
        // return [
        // [(cnpImage.getWidth()/2) * 1 + 7, group.getOffset().y ],
        // [(cnpImage.getWidth()/2) * 1+ Math.max (dis, 7), group.getOffset().y]
        controlGroup.setDragBoundFunc(function (pos) {
            var groupPos = group.getPosition()
            var rotation = degrees(angle(groupPos.x, groupPos.y, pos.x, pos.y))
            var dis = distance(groupPos.x, groupPos.y, pos.x, pos.y)
            group.setRotationDeg(rotation)
            layer.draw()
            return pos
        })
        controlGroup.on('dragend', function () {
            controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
            //if (!rotate) line.setPoints (linePoints (-1*(cnpImage.getWidth()/2)))
            layer.draw()
        })
        var signOpacity = 0;
        var animationTick = 0
        var signOpacityAnimation = new Kinetic.Animation(function (frame) {
            var opacity = controlGroup.getOpacity()
            if (opacity == signOpacity) {
                signOpacityAnimation.stop();
                return
            }
            if (opacity < signOpacity) opacity += frame.timeDiff / 200;
            else opacity -= frame.timeDiff / 200
            if (opacity < 0) opacity = 0;
            if (opacity > 1) opacity = 1
            controlGroup.setOpacity(opacity)
        }, layer);

        function calcSignOpacity() {
            if (rotate) {
                if ( store.attrs.x == 1) { 
                    console.warn(7)
                    px = cnpImage.getAbsolutePosition().x
                    py = cnpImage.getAbsolutePosition().y
                    group.setPosition(px, py)
                    cnpImage.setPosition(0, 0)
                    controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
                    store.attrs.x = 0;
                }

                var mousePos = stage.getMousePosition()
                if (mousePos) {
                    var controlPos = controlGroup.getPosition()
                    var groupPos = group.getPosition()
                    var dis = Math.min(
                        distance(mousePos.x, mousePos.y, controlPos.x, controlPos.y),
                        distance(mousePos.x, mousePos.y, groupPos.x, groupPos.y))
                    var ldis = distance(groupPos.x, groupPos.y, controlPos.x, controlPos.y)
                    line.setPoints(linePoints(100-17)) // fix distance
                    controlHOME.setPosition((cnpImage.getWidth() / 2) * 1 + Math.max(100, 7), group.getOffset().y)
                    stage.get('.control').each(function(i){ (i.attrs.opacity = 100) })
                    signOpacity = 10
                    line.setOpacity(50)
                }
                if (controlGroup.getOpacity() != signOpacity && !signOpacityAnimation.isRunning()) signOpacityAnimation.start()
            } else {
                stage.get('.control').each(function(i){ (i.attrs.opacity = 0) })
                if ( store.x) { 
                    px = cnpImage.getAbsolutePosition().x
                    py = cnpImage.getAbsolutePosition().y
                    group.setPosition(px, py)
                    cnpImage.setPosition(0, 0)
                    controlGroup.setPosition(controlHOME.getAbsolutePosition().x, controlHOME.getAbsolutePosition().y)
                    store.x = 0;
                }

                signOpacity = 0
                line.setOpacity(0)
            }
        }
        stage.getContainer().addEventListener('mousemove', calcSignOpacity, false);
        layer.draw();
    }

    function drawRoad(imageObj, d_w, d_h, ox, oy, ly) {
        ofy = parseInt(d_w / 2);
        ofx = parseInt(d_h / 2);
        var cnpRoad = new Kinetic.Image({
            image: imageObj,
            x: 402,
            y: 307,
            width: d_w,
            height: d_h,
            offset: [ofx, ofy],
            dragOnTop: false,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
            shadowOpacity: 0.5,
            shadowEnabled: true,
            strokeWidth: 2,
            snap: true,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                if (rotate) {
                    var mouse = stage.getMousePosition();
                    var xd = mouse.x;
                    var yd = mouse.y;
                    var theta = Math.atan2(yd, xd);
                    var degree = (theta / (Math.PI / 180)) * 4;
                    if (!isDragging) {
                        isDragging = true;
                        refRotation = degree;
                    } else {
                        this.setRotationDeg(degree - refRotation);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });
        cnpRoad.on('mouseover', function () {
            document.body.style.cursor = 'pointer';
            this.setStrokeWidth(2);
            layer.draw();
        });
        cnpRoad.on('mouseout', function () {
            document.body.style.cursor = 'default';
            this.setStrokeWidth(0);
            layer.draw();
        });
        cnpRoad.on('click', function () {
            if (remove == 1) this.remove()
            this.setStrokeWidth(0);
            this.moveToBottom();
            layer.draw();
        });
        cnpRoad.on('dragend', function () {
            if (rotate) {
                isDragging = false;
            } else {
                isDragging = true;
            }
            layer.draw();
        });
        layer.add(cnpRoad);
        stage.add(layer);
        layer.draw();
    }

    function drawObstacles(imageObj, d_w, d_h, ox, oy, ly) {
        ofy = parseInt(d_w / 2);
        ofx = parseInt(d_h / 2);
        var cnpObstacles = new Kinetic.Image({
            image: imageObj,
            x: 402,
            y: 307,
            width: d_w,
            height: d_h,
            offset: [ofx, ofy],
            dragOnTop: false,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
            shadowOpacity: 0.5,
            shadowEnabled: true,
            strokeWidth: 2,
            snap: true,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                if (rotate) {
                    var mouse = stage.getMousePosition();
                    var xd = mouse.x;
                    var yd = mouse.y;
                    var theta = Math.atan2(yd, xd);
                    var degree = theta / (Math.PI / 180);
                    if (!isDragging) {
                        isDragging = true;
                        refRotation = degree;
                    } else {
                        this.setRotationDeg(degree - refRotation);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });
        cnpObstacles.on('mouseover', function () {
            document.body.style.cursor = 'pointer';
            this.setStrokeWidth(2);
        });
        cnpObstacles.on('mouseout', function () {
            document.body.style.cursor = 'default';
            this.setStrokeWidth(0);
        });
        cnpObstacles.on('click', function () {
            if (remove == 1) this.remove()
            this.setStrokeWidth(0);
            this.moveToBottom();
            this.moveUp();
            layer.draw();
        });
        cnpObstacles.on('dragend', function () {
            if (rotate) {
                isDragging = false;
            } else {
                isDragging = true;
            }
            layer.draw();
        });
        layer.add(cnpObstacles);
        stage.add(layer);
        layer.draw();
    }

    function createLabel(label_text, label_dir) {
        var tooltip = new Kinetic.Label({
            x: (stage.getWidth() / 2),
            y: (stage.getHeight() / 2),
            opacity: 0.75,
            draggable: true
        });
        tooltip.add(new Kinetic.Tag({
            fill: 'black',
            pointerDirection: label_dir,
            pointerWidth: 10,
            pointerHeight: 10,
            lineJoin: 'round',
            shadowColor: 'black',
            shadowOpacity: 0.5
        }));
        tooltip.add(new Kinetic.Text({
            text: label_text,
            fontFamily: 'Calibri',
            fontSize: 18,
            padding: 5,
            fill: 'white'
        }));
        tooltip.on('click', function () {
            if (remove == 1) this.remove()
            layer.draw();
        });
        layer.add(tooltip);
        layer.draw();
    }