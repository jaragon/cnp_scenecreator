$(window).on('load', function() {

  var isMobile = false;
  rotateHandlerRadius = 7;

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    isMobile = true;
    rotateHandlerRadius = 10;
    $('body').addClass('fat-scrollbar');
    $('body').addClass('isMobile');
  }

  Kinetic.angleDeg = false;

  stage = new Kinetic.Stage({
    container: 'space',
    width: 804,
    height: 614,
    objId: 0
  });

  layer = new Kinetic.Layer({
    draggable: false,
    id: 'layer'
  });


  bgGroup = new Kinetic.Group({
    id: 'bgGroup'
  });

  mainGroup = new Kinetic.Group({
    draggable: true,
    id: 'mainGroup'
  });

  floor = new Kinetic.Rect({
    fill: 'transparent',
    width: 8000,
    height: 8000,
    x: -4000,
    y: -4000,
    id: 'floor'
  });

  floor.on('click', function(e) {
    activeObject = null;
    stage.find('.rotateTool').each(function(node) {
      node.hide();
    });
    stage.draw();
    $colorPickerTool.fadeOut();
    e.cancelBubble = true;
  }, false);

  mainGroup.add(floor);  

  roadGroup = new Kinetic.Group({
    id: 'roadGroup'
  });
  mainGroup.add(roadGroup);

  objectGroup = new Kinetic.Group({
    id: 'objectGroup'
  });
  mainGroup.add(objectGroup);

  layer.add(bgGroup);
  layer.add(mainGroup);

  stage.add(layer);

  $('#side_bar_menu02 img').draggable({
    helper: 'clone',
    appendTo: 'body'
  });

  $('#space').droppable({
    hoverClass: 'over',
    drop: dragDrop
  });

  $('.cnp-background-delete').on('click mouseup', function() {
    stage.find('.image2').each(function(node) {
      node.destroy();
      layer.draw();
      makeHistory();
    });
  });

  $('.cnp-delete-tool').on('click mouseup', function() {
    activeObject.destroy();
    layer.draw();
    makeHistory();
  });
  $('.scobjVehicles img').each(function() {
    $(this).attr('data-type', 'vehicles');
  });
  $('.scobjObstacles img').each(function() {
    $(this).attr('data-type', 'obstacles');
  });
  $('.scobjPeople img').each(function() {
    $(this).attr('data-type', 'people');
  });
  $('.scobjDamage img').each(function() {
    $(this).attr('data-type', 'vehicles');
  });
  $('.scobjStreets img').each(function() {
    $(this).attr('data-type', 'roads');
  });
  $('.scobjRoadSigns img').each(function() {
    $(this).attr('data-type', 'signs');
  });

  $colorPickerTool = $('.cnp-color-picker-tool').hide();

  stage.objId = 0;
  
  //var activeObject;

  addLabel = function(label_text, label_dir, x, y, rot) {
    var img = new Kinetic.Label({
      x: 0,
      y: 0,
      opacity: 0.75,
      name: 'itemLabelImg',
      id: stage.objId + 1
    });

    img.add(new Kinetic.Tag({
      fill: 'black',
      pointerDirection: label_dir,
      pointerWidth: 10,
      pointerHeight: 10,
      lineJoin: 'round',
      shadowColor: 'black',
      shadowOpacity: 0.5,
      name: 'labelDir'
    }));

    img.add(new Kinetic.Text({
      text: label_text,
      fontFamily: 'Calibri',
      fontSize: 18,
      padding: 5,
      fill: 'white',
      name: 'labelText'
    }));


    img.setOffset({
      x: (label_dir == "left") ? img.getWidth() / 2 : -img.getWidth() / 2,
      y: img.getHeight() / 2 - 12
    });

    var imgGroup = new Kinetic.Group({
        x: x || (stage.getWidth() / 2),
        y: y || (stage.getHeight() / 2),
        //x: x + img.getWidth() / 2,
        //y: y + img.getHeight() / 2,
        name: 'itemLabel',
        draggable: true,
        stroke: 'red',
        strokeWidth: 1,
        rotation: rot || 0
    });

    imgGroup.add(img);

    imgGroup.on('dblclick', function(e) {
      var label_dir = imgGroup.find('.labelDir')[0].attrs.pointerDirection;
      var label_text = imgGroup.find('.labelText')[0].attrs.text;
      var x = imgGroup.getX();
      var y = imgGroup.getY();
      var rot = imgGroup.getRotation();

      var label_text_new = prompt("Enter new label", label_text);

      addLabel(label_text_new, label_dir, x, y, rot);

      imgGroup.destroy();

      stage.draw();

      //$('#signage input').val(labelText);
      //$('#signage').toggle('fast');

    });

    imgGroup.on('click dragstart', function(e) {
      activateObject(img);
    });

    imgGroup.on('dragend', function(e) {
      makeHistory();
      e.cancelBubble = true;
    }, false);

    var radius = Math.sqrt(Math.pow(img.getWidth(), 2) + Math.pow(img.getHeight(), 2)) / 2;

    var rotateTool = new Kinetic.Group({
      name: 'rotateTool'
    });

    imgGroup.add(rotateTool);

    var rotateBorder = new Kinetic.Circle({
      x: 0,
      y: 0,
      radius: radius + 10,
      stroke: '#FFB129',
      strokeWidth: 2,
      name: 'rotateBorder'
    });
    
    rotateTool.add(rotateBorder);

    var rotateHandler = new Kinetic.Circle({
      x: (label_dir == "left") ? img.getX() + img.getWidth() / 2 - img.getOffset().x : img.getX() + img.getWidth() / 2 + img.getOffset().x,
      y: img.getY() - radius - 10,
      radius: rotateHandlerRadius,
      fill: '#FFB129',
      stroke: '#000',
      strokeWidth: 3,
      draggable: true,
      name: 'rotateHandler',
      dragBoundFunc: function(pos) {
        if (this.isDragging()) {
          var p = imgGroup.getAbsolutePosition();
          var v = {x: p.x - pos.x, y: p.y - pos.y};
          angle = Math.atan2(v.y, v.x) - Math.PI / 2;

          imgGroup.setRotation(angle);
        }
        return pos;
      }
    });

    rotateHandler.on('dragmove', function(e) {
      var targetX = (label_dir == "left") ? img.getX() - img.getOffsetX() : img.getX() + img.getOffsetX(); 
      var targetY = img.getY() - img.getOffsetY();
      var targetWidth = img.getWidth();
      var targetHeight = img.getHeight();
      
      var rotate = {
        x: targetX + targetWidth / 2,
        y: img.getY() - radius - 10
      };

      rotateHandler.setPosition(rotate);

      img.setPosition({x: 0, y: 0});
      
      //img.setOffset({
          //x: (label_dir == "left") ? img.getWidth() / 2 : -img.getWidth() / 2,
          //y: img.getHeight() / 2
      //});

      e.cancelBubble = true;
    }, false);

    rotateTool.add(rotateHandler);

    rotateHandler.on('dragend', function(e) {
      makeHistory();
      e.cancelBubble = true;
    }, false);

    // activate object
    stage.find('.rotateTool').each(function(node) {
      node.hide();
    });

    activeObject = imgGroup;

    imgGroup.find('.rotateTool')[0].show();

    imgGroup.add(img);

    objectGroup.add(imgGroup);

    stage.draw();

    stage.objId++;

    makeHistory();

  }

  addImage = function(obj) {
    
    //remove bg-image if existing
    if (stage.find('.image2')[0]) {
      stage.find('.image2').each(function(node) {
        var imgObj = new Image();
        imgObj.onload = function() {
          node.setImage(imgObj);
          stage.draw();
          makeHistory();
        }
        node.attrs.imgsrc = obj;
        imgObj.src = obj;
      });
    }

    else {
      var bgGroup = stage.find('#bgGroup')[0];
      var imgObj = new Image();
      var img = new Kinetic.Image({ 
        x: 0,
        y: 0,
        image: imgObj,
        stroke: 'transparent',
        strokeWidth: 1,
        name: 'image2',
        imgsrc: obj
      });

      bgGroup.add(img);

      imgObj.onload = function() {
        img.setImage(imgObj);
        stage.draw();
        makeHistory();
      }

      imgObj.src = obj;
    }

  }

  addObject = function(obj, type, x, y, basename, dir) {
    var imgObj = new Image();
    var img, imgGroup;
    var angle = 0;

    var group = (type == "roads") ? roadGroup : objectGroup;

    imgObj.onload = function() {
      img = new Kinetic.Image({
        x: 0,
        y: 0,
        image: imgObj,
        stroke: 'transparent',
        strokeWidth: 1,
        name: 'image',
        imgsrc: obj
      });


      img.setOffset({
          x: img.getWidth() / 2,
          y: img.getHeight() / 2
      });

      var imgGroup = new Kinetic.Group({
          x: x + img.getWidth() / 2,
          y: y + img.getHeight() / 2,
          name: 'item',
          draggable: true,
          type: type
      });

      imgGroup.on('click dragstart touchstart', function(e) {
        activateObject(img);
      });

      imgGroup.on('dragend', function(e) {
        makeHistory();
        e.cancelBubble = true;
      }, false);

      imgGroup.on('dragmove', function() {
        // snap-to-grid
        if (imgGroup.attrs.type == "roads") {
          imgGroup.setPosition({ 
            x: Math.floor(imgGroup.getPosition().x/20) * 20 + 20,
            y: Math.floor(imgGroup.getPosition().y/20) * 20 + 20
          });
        }
      });

      var radius = Math.sqrt(Math.pow(img.getWidth(), 2) + Math.pow(img.getHeight(), 2)) / 2;

      var rotateTool = new Kinetic.Group({
        name: 'rotateTool'
      });

      imgGroup.add(rotateTool);

      var rotateBorder = new Kinetic.Circle({
        x: 0,
        y: 0,
        radius: radius,
        stroke: '#FFB129',
        strokeWidth: 2,
        name: 'rotateBorder'
      });
      
      rotateTool.add(rotateBorder);

      var rotateHandler = new Kinetic.Circle({
        x: img.getX() + img.getWidth() / 2 - img.getOffset().x,
        y: img.getY() - radius,
        radius: rotateHandlerRadius,
        fill: '#FFB129',
        stroke: '#000',
        strokeWidth: 3,
        draggable: true,
        name: 'rotateHandler',
        dragBoundFunc: function(pos) {
          if (this.isDragging()) {
            var p = imgGroup.getAbsolutePosition();
            var v = {x: p.x - pos.x, y: p.y - pos.y};
            angle = Math.atan2(v.y, v.x) - Math.PI / 2;

            imgGroup.setRotation(angle);
          }
          return pos;
        }
      });

      rotateHandler.on('dragmove', function(e) {
        var targetX = img.getX() - img.getOffsetX();
        var targetY = img.getY() - img.getOffsetY();
        var targetWidth = img.getWidth();
        var targetHeight = img.getHeight();
        
        var rotate = {
          x: targetX + targetWidth / 2,
          y: img.getY() - radius
        };

        rotateHandler.setPosition(rotate);

        img.setPosition({x: 0, y: 0});
        
        img.setOffset({
            x: img.getWidth() / 2,
            y: img.getHeight() / 2
        });

        e.cancelBubble = true;
      }, false);

      rotateTool.add(rotateHandler);


      rotateHandler.on('dragend', function(e) {
        makeHistory();
        e.cancelBubble = true;
      }, false);

      // activate object
      stage.find('.rotateTool').each(function(node) {
        node.hide();
      });

      activeObject = imgGroup;

      imgGroup.find('.rotateTool')[0].show();
      
      // set type

      imgGroup.attrs.type = type;
      imgGroup.attrs.basename = basename;
      imgGroup.attrs.dir = dir;

      if (type == "vehicles" || type == "people")  {
        var re = /(impact)/;
        if (re.test(obj))
          imgGroup.attrs.type = "misc";
      }

      if (imgGroup.attrs.type == "vehicles" || imgGroup.attrs.type == "people") 
        $colorPickerTool.fadeIn();
      else 
        $colorPickerTool.fadeOut();

      imgGroup.add(img);
      group.add(imgGroup);
      stage.draw();
    }

    stage.objId++;

    imgObj.src = obj;

    makeHistory();

  }


  function dragDrop(e, ui) {
    var element = ui.draggable;
    var data = $(element).attr('src');
    var type = $(element).attr('data-type');

    var basename = $(element).attr('data-image');
    var dir = 'scobj/' + $(element).attr('data-type') + '/';
    var data = dir + basename + '.png';

    var canvasOffset = $('#space').offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    var x = ((e.clientX - offsetX) - mainGroup.position().x) / scale;
    var y = ((e.clientY - offsetY - $(element).height() / 2) - mainGroup.position().y) / scale;

    addObject(data, type, x, y, basename, dir);
  }

  var scale = 1;
  var $zoomCurrent = $('.cnp-zoom-current');

  function zoom(x) {
    currentZoom = parseInt(scale * 100);
    console.log(currentZoom);
    // need to refactor/simplify
    if (currentZoom == 20 && x > 0) {
      scale += x;
      mainGroup.setScale({x: scale, y: scale});
    }
    else if (currentZoom == 200 && x < 0) {
      scale += x;
      mainGroup.setScale({x: scale, y: scale});
    }
    else if (currentZoom > 20 && currentZoom < 200) {
      scale += x;
      mainGroup.setScale({x: scale, y: scale});
    }
    $zoomCurrent.text(currentZoom + '%');
    stage.draw();
    makeHistory();
  }

  function changeColor(color) {
    var obj = activeObject.find('.image')[0];
    var img = new Image();

    switch (color) {
      case 'red':
        color = '-red.png';
        break;
      case 'blue':
        color = '-blue.png';
        break;
      case 'green':
        color = '-green.png';
        break;
      default:
        color = '.png';
        break;
    }

    img.onload = function() {
      obj.setImage(img);
      layer.draw();
    }

    img.src = activeObject.attrs.dir + activeObject.attrs.basename + color;

    obj.attrs.imgsrc = activeObject.attrs.dir + activeObject.attrs.basename + color;

    makeHistory();

  }

  $('.cnp-zoom-in').on('click', function() {
      zoom(0.1);  
      $colorPickerTool.fadeOut();
      return false;
  });

  $('.cnp-zoom-out').on('click', function() {
      zoom(-0.1);
      $colorPickerTool.fadeOut();
      return false;
  });

  $('.cnp-zoom-reset').on('click', function() {
    scale = 1;
    mainGroup.setScale({x: scale, y: scale});
    mainGroup.position({x: 0, y: 0});
    $zoomCurrent.text(parseInt(scale * 100) + '%');
    stage.draw();
    $colorPickerTool.hide();
    return false;
  });


  $('.cnp-pan').on('click', function() {
    $('body').removeClass('display-help');
    $(this).toggleClass('hover');
    $(this).toggleClass('cnp-mouse-move');
    $('.kineticjs-content').toggleClass('cnp-mouse-move');

    var tempLayer = new Kinetic.Layer();
    stage.add(tempLayer);

    stage.find('#bgGroup').each(function(node) {
      if (node.draggable() == true) {
        node.draggable(false);
        $('.cnp-zoom-image-tool').removeClass('active');
        stage.find('#panner')[0].destroy();
      }
      else if (node.draggable() == false) {
        node.draggable(true);
        $('.cnp-zoom-image-tool').addClass('active');
        var panner = new Kinetic.Rect({
          fill: 'red',
          opacity: 0.2,
          width: 8000,
          height: 8000,
          x: layer.position().x - 4000,
          y: layer.position().y -4000,
          id: 'panner',
          draggable: true
        });

        panner.on('dragmove', function(e) {
          node.setAbsolutePosition({ x: this.x() + 4000, y: this.y() + 4000 });
          layer.draw();
          e.cancelBubble = true;
        }, false);

        tempLayer.add(panner);
      }
    });
    stage.draw();
    makeHistory();
  });

  var imgscale = 1;

  function zoomImage(x) {
    bgGroup = stage.find('#bgGroup')[0];
    imgscale += x;

    bgGroup.scale({ x: imgscale, y: imgscale });

    stage.draw();
    makeHistory();
  }
  $('.cnp-zoom-image-in').on('click', function() {
      zoomImage(0.1);
  });
  $('.cnp-zoom-image-out').on('click', function() {
      zoomImage(-0.1);
  });

  $('#create_label').live('click', function (e) {
      alert('Click to drop label');
      $(this).attr('disabled', true);
      var tempLayer = new Kinetic.Layer({ id: 'tempLayer' });
      stage.add(tempLayer);

      var clicker = new Kinetic.Rect({
        fill: 'blue',
        opacity: 0.1,
        width: stage.width(),
        height: stage.height(),
        id: 'panner',
        draggable: false
      });

      clicker.on('click touchstart', function(e) {
        var canvasOffset = $('#space').offset();
        var offsetX = canvasOffset.left;
        var offsetY = canvasOffset.top;
        var x = ((e.evt.clientX - offsetX) - mainGroup.position().x) / scale;
        var y = ((e.evt.clientY - offsetY) - mainGroup.position().y) / scale;
        addLabel($('#label_text').val(), $('#label_direction').val(), x, y);
        e.cancelBubble = true;
        tempLayer.destroy();
        $('#signage').toggle('fast');
        $('#signage input').val('');
        $('#create_label').removeAttr('disabled');
        makeHistory();
      }, false);

      tempLayer.add(clicker);

      stage.draw();
      $colorPickerTool.fadeOut();
      return false;
  });

  $('#cancel_label').live('click', function (e) {
    stage.find('#tempLayer')[0].destroy();
    $('#signage').toggle('fast');
    $('#signage input').val('');
    $('#create_label').removeAttr('disabled');
  });

  $(".cnp-color-picker-tool-plaintiff").on('click', function () {
      changeColor('green');
      return false;
  });

  $(".cnp-color-picker-tool-defendant").on('click', function () {
      changeColor('red');
      return false;
  });

  $(".cnp-color-picker-tool-neutral").on('click', function () {
      changeColor('blue');
      return false;
  });

  $('.cnp-undo-tool').on('click', function() {
    undoHistory();
    $colorPickerTool.fadeOut();
      return false;
  });

  $('.cnp-redo-tool').on('click', function() {
    redoHistory();
    $colorPickerTool.fadeOut();
      return false;
  });

  $('.cnp-save').on('click', function() {
    addImage('./images/accident-scene.jpg');
    console.log(layer.toJSON());
  });

  $('.cnp-cancel-tool').on('click', function() {
    loadJson(layer.toJSON());
  });

  $('.accordionButton').on('click', function() {
    $colorPickerTool.fadeOut();
  });

  $('.cnp-help').on('click', function() {
    $('body').toggleClass('display-help');
  });


});

// from old code

function spaceBackground(d) {
  result = d.match(/uploads\/(.*)"\s/img);
  var bkg = result[0];
  background = bkg.replace(/"/img, "");
  $('#space').css('background', 'url(' + background + ')');
}

function toggle_upload() {
  $('#upload').toggle('fast');
}

function toggle_signage() {
  $('#signage').toggle('fast');
}

function export_image() {
  stage.toDataURL({
    width: 804,
    height: 614,
    mimeType: "image/png",
    callback: function (dataUrl) {
      window.open(dataUrl);
    }
  });
}

function saveJson() {
  console.log(layer.toJSON());
}

function loadJson(json) {
  layer.destroy();
  layer = Kinetic.Node.create(json, 'space');
  stage.add(layer);
  floor = stage.find('#floor')[0];
  mainGroup = stage.find('#mainGroup')[0];
  roadGroup = stage.find('#roadGroup')[0];
  objectGroup = stage.find('#objectGroup')[0];

  activeObject = null;
  stage.find('.rotateTool').each(function(node) {
    node.hide();
  });

  stage.find('#floor').each(function(node) {
    node.on('click', function(e) {
      activeObject = null;
      stage.find('.rotateTool').each(function(node) {
        node.hide();
      });
      stage.draw();
      $colorPickerTool.fadeOut();
      e.cancelBubble = true;
    }, false);
  });

  stage.find('.image2').each(function(node) {
    var imgNew = new Image();
    imgNew.onload = function() {
      node.image(imgNew);
      stage.draw();
    }

    imgNew.src = node.attrs.imgsrc;
  });
  stage.find('.image').each(function(node) {
    var imgGroup = node.getParent();
    var imgNew = new Image();
    imgNew.onload = function() {
      node.image(imgNew);

      var img = imgGroup.find('Image')[0];
      var rotateHandler = imgGroup.find('.rotateHandler')[0];
      var rotateBorder = imgGroup.find('.rotateBorder')[0];
      var radius = Math.sqrt(Math.pow(img.getWidth(), 2) + Math.pow(img.getHeight(), 2)) / 2;


      rotateHandler.setRadius(rotateHandlerRadius);
      
      imgGroup.on('click dragstart touchstart', function(e) {
        activateObject(img);
      });

      imgGroup.on('dragend', function(e) {
        makeHistory();
        e.cancelBubble = true;
      }, false);

      // snap-to-grid for roads
      imgGroup.on('dragmove', function() {
        if (imgGroup.type == "roads") {
          imgGroup.setPosition({ 
            x: Math.floor(imgGroup.getPosition().x/20) * 20 + 20,
            y: Math.floor(imgGroup.getPosition().y/20) * 20 + 20
          });
        }
      });

      rotateHandler.on('dragmove', function(e) {
        var targetX = img.getX() - img.getOffsetX();
        var targetY = img.getY() - img.getOffsetY();
        var targetWidth = img.getWidth();
        var targetHeight = img.getHeight();
        
        var rotate = {
          x: targetX + targetWidth / 2,
          y: img.getY() - radius
        };

        rotateHandler.setPosition(rotate);

        img.setPosition({x: 0, y: 0});
        
        img.setOffset({
            x: img.getWidth() / 2,
            y: img.getHeight() / 2
        });

        e.cancelBubble = true;
      }, false);

      rotateHandler.on('dragend', function(e) {
        makeHistory();
        e.cancelBubble = true;
      }, false);

      rotateHandler.dragBoundFunc(function(pos) {
        if (this.isDragging()) {
          var p = imgGroup.getAbsolutePosition();
          var v = {x: p.x - pos.x, y: p.y - pos.y};
          angle = Math.atan2(v.y, v.x) - Math.PI / 2;

          imgGroup.setRotation(angle);
        }
        return pos;
      });
      stage.draw();
    }
    imgNew.src = node.attrs.imgsrc;
    stage.draw();
  });

  stage.find('.itemLabel').each(function(imgGroup) {

    imgGroup.on('dblclick', function(e) {
      var label_dir = imgGroup.find('.labelDir')[0].attrs.pointerDirection;
      var label_text = imgGroup.find('.labelText')[0].attrs.text;
      var x = imgGroup.getX();
      var y = imgGroup.getY();
      var rot = imgGroup.getRotation();

      var label_text_new = prompt("Enter new label", label_text);

      addLabel(label_text_new, label_dir, x, y, rot);

      imgGroup.destroy();

      stage.draw();

    });

    var img = imgGroup.find('.itemLabelImg')[0];
    var rotateHandler = imgGroup.find('.rotateHandler')[0];
    var rotateBorder = imgGroup.find('.rotateBorder')[0];
    var radius = Math.sqrt(Math.pow(img.getWidth(), 2) + Math.pow(img.getHeight(), 2)) / 2;
    var label_dir = imgGroup.find('.labelDir')[0].attrs.pointerDirection;

    rotateHandler.setRadius(rotateHandlerRadius);

    imgGroup.on('click dragstart touchstart', function(e) {
      activateObject(img);
    });

    imgGroup.on('dragend', function(e) {
      makeHistory();
      e.cancelBubble = true;
    }, false);

    rotateHandler.on('dragmove', function(e) {
      var targetX = (label_dir == "left") ? img.getX() - img.getOffsetX() : img.getX() + img.getOffsetX();
      var targetY = img.getY() - img.getOffsetY();
      var targetWidth = img.getWidth();
      var targetHeight = img.getHeight();

      var rotate = {
        x: targetX + targetWidth / 2,
        y: img.getY() - radius - 10
      };

      rotateHandler.setPosition(rotate);

      img.setPosition({x: 0, y: 0});

      e.cancelBubble = true;
    }, false);

    rotateHandler.on('dragend', function(e) {
      makeHistory();
      e.cancelBubble = true;
    }, false);

    rotateHandler.dragBoundFunc(function(pos) {
      if (this.isDragging()) {
        var p = imgGroup.getAbsolutePosition();
        var v = {x: p.x - pos.x, y: p.y - pos.y};
        angle = Math.atan2(v.y, v.x) - Math.PI / 2;

        imgGroup.setRotation(angle);
      }
      return pos;
    });

    stage.draw();
  });

  stage.draw();
}

var historyDb = [];
var historyStep = 0;

function makeHistory() {
  historyStep++;
  historyDb.push(layer.toJSON());
  historyDb = historyDb.slice(0, historyStep);
}

function undoHistory() {
  if (historyStep > 0) {
    historyStep--;
    loadJson(historyDb[historyStep])
  }
}

function redoHistory() {
  if (historyStep < historyDb.length-1) {
    historyStep++;
    loadJson(historyDb[historyStep]);
    stage.draw();
  }
}

function activateObject(obj) {
  stage.find('.rotateTool').each(function(node) {
    node.hide();
  });

  activeObject = obj.getParent();

  obj.getParent().find('.rotateTool')[0].show();
  
  if (activeObject.attrs.type == "vehicles" || activeObject.attrs.type == "people")  
    $colorPickerTool.fadeIn();
  else 
    $colorPickerTool.fadeOut();

  stage.draw();
}

function relaa() {
  var json = '{"attrs":{"id":"layer"},"className":"Layer","children":[{"attrs":{"id":"bgGroup"},"className":"Group","children":[{"attrs":{"stroke":"transparent","strokeWidth":1,"name":"image2","imgsrc":"./images/sample.png"},"className":"Image"}]},{"attrs":{"draggable":true, "id":"mainGroup"},"className":"Group","children":[{"attrs":{"fill":"transparent","width":8000,"height":8000,"x":-4000,"y":-4000,"id":"floor"},"className":"Rect"},{"attrs":{"id":"roadGroup"},"className":"Group","children":[]},{"attrs":{"id":"objectGroup"},"className":"Group","children":[]}]}]}';
  loadJson(json);
}

function previewImage(img) {
  if (img.files && img.files[0]) {
    var reader = new FileReader();
      reader.onload = function(e) {
        addImage(e.target.result);
      }
    reader.readAsDataURL(img.files[0]);
  }
}

function updateImageSourceOnLoad(obj) {
  if (stage.find('.image2')[0]) {
    stage.find('.image2').each(function(node) {
      var imgObj = new Image();
      imgObj.onload = function() {
        node.setImage(imgObj);
        stage.draw();
        makeHistory();
      }
      node.attrs.imgsrc = obj;
      imgObj.src = obj;
    });
  }
}

// jquery toggle plugin

$.fn.toggleClick = function() {
  var methods = arguments;    
  var count = methods.length; 

  return this.each(function(i, item){
    var index = 0;
    $(item).click(function() {
      return methods[index++ % count].apply(this, arguments);
    });
  });
};
