
    function rotate(angle){ // 90 or -90
        if(stage.getHeight() <= image.getWidth()){ 
            aspect = image.getWidth() / image.getHeight();
            height = stage.getHeight() / aspect;
            image.setWidth(stage.getHeight());
            image.setHeight(height);
        }
        image.setOffset(image.getWidth()/2,image.getHeight()/2);
        image.setPosition(stage.getWidth()/2,stage.getHeight()/2);
        image.rotateDeg(angle);
        layer.draw();
    }

    var stage = new Kinetic.Stage({
        container: 'space',
        width: 804,
        height: 614,
        draggable: true
    });

    var layer = new Kinetic.Layer();
    var isDragging = false;
    var refRotation = null;


    function drawImage(imageObj, d_w, d_h, ox, oy, ly) {
        ofy = parseInt(d_w / 2 );
        ofx = parseInt(d_h / 2 );

        var cnpImage = new Kinetic.Image({
            image: imageObj,
            x: 402,
            y: 307,
            width: d_w,
            height: d_h,
            offset: [ ofx, ofy ],
            dragOnTop: true,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
        strokeWidth: 2,
            //shadowOffset: 10,
            shadowOpacity: 0.5,            
            shadowEnabled: true,
            snap: true,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                if (rotate) {
                    var mouse = stage.getMousePosition();
                    var xd =  mouse.x ;
                    var yd =  mouse.y ;
                    var theta = Math.atan2(yd, xd);
                    var degree = theta / (Math.PI / 360);

                    if(!isDragging) {
                        isDragging = true;
                        refRotation = degree;
                    } else {
                        this.setRotationDeg(degree - refRotation);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });

        cnpImage.on('mouseover', function() {
          document.body.style.cursor = 'pointer';
        });
        cnpImage.on('mouseout', function() {
          document.body.style.cursor = 'default';
        });

        cnpImage.on('click', function() {
            if ( remove == 1) this.remove()
            this.setShadowOffset(0);
            layer.draw();
        });

        cnpImage.on('dragend', function() {
            if (rotate) {
                isDragging = false;
            } else {
                isDragging = true;
            }
            layer.draw();
        });

        layer.add(cnpImage);
        stage.add(layer);
        layer.draw();

    }

    function drawRoad(imageObj, d_w, d_h, ox, oy, ly) {
        ofy = parseInt(d_w / 2 );
        ofx = parseInt(d_h / 2 );
        var cnpRoad = new Kinetic.Image({
            image: imageObj,
            x: 402,
            y: 307,
            width: d_w,
            height: d_h,
            offset: [ ofx, ofy ],
            dragOnTop: false,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
            //shadowOffset: 10,
            shadowOpacity: 0.5,            
            shadowEnabled: true,
        strokeWidth: 2,
            snap: true,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                if (rotate) {
                    var mouse = stage.getMousePosition();
                    var xd =  mouse.x ;
                    var yd =  mouse.y ;
                    var theta = Math.atan2(yd, xd);
                    var degree = (theta / (Math.PI / 180)) * 4;

                    if(!isDragging) {
                        isDragging = true;
                        refRotation = degree;
                        console(1);
                    } else {
                        console(2);
                        console.warn( degree  + ' -- ' + refRotation);
                        this.setRotationDeg(degree - refRotation);
                        //this.setRotationDeg(degree );
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });

        cnpRoad.on('mouseover', function() {
          document.body.style.cursor = 'pointer';
        });
        cnpRoad.on('mouseout', function() {
          document.body.style.cursor = 'default';
        });
        cnpRoad.on('click', function() {
            if ( remove == 1) this.remove()
            this.setShadowOffset(0);
            this.moveToBottom();
            layer.draw();
        });
        cnpRoad.on('dragend', function() {
            if (rotate) {
                isDragging = false;
            } else {
                isDragging = true;
            }
            layer.draw();
        });

        layer.add(cnpRoad);
        stage.add(layer);
        layer.draw();
    }

    function drawObstacles(imageObj, d_w, d_h, ox, oy, ly) {
        ofy = parseInt(d_w / 2 );
        ofx = parseInt(d_h / 2 );
        var cnpObstacles = new Kinetic.Image({
            image: imageObj,
            x: 402,
            y: 307,
            width: d_w,
            height: d_h,
            offset: [ ofx, ofy ],
            dragOnTop: false,
            draggable: true,
            shadowColor: 'black',
            shadowBlur: 2,
            //shadowOffset: 10,
            shadowOpacity: 0.5,            
            shadowEnabled: true,
            strokeWidth: 2,
            snap: true,
            rotationDeg: 0,
            dragBoundFunc: function (pos) {
                if (rotate) {
                    var mouse = stage.getMousePosition();
                    var xd =  mouse.x ;
                    var yd =  mouse.y ;
                    var theta = Math.atan2(yd, xd);
                    var degree = theta / (Math.PI / 180);

                    if(!isDragging) {
                        isDragging = true;
                        refRotation = degree;
                    } else {
                        this.setRotationDeg(degree - refRotation);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: this.getAbsolutePosition().y
                    }
                } else {
                    return {
                        x: pos.x,
                        y: pos.y
                    }
                }
            }
        });

        cnpObstacles.on('mouseover', function() {
          document.body.style.cursor = 'pointer';
        });
        cnpObstacles.on('mouseout', function() {
          document.body.style.cursor = 'default';
        });
        cnpObstacles.on('click', function() {
            if ( remove == 1) this.remove()
            this.setShadowOffset(0);
            this.moveToBottom();
            this.moveUp();
            layer.draw();
        });
        cnpObstacles.on('dragend', function() {
            if (rotate) {
                isDragging = false;
            } else {
                isDragging = true;
            }
            layer.draw();
        });

        layer.add(cnpObstacles);
        stage.add(layer);
        layer.draw();
    }

