<?

?><!DOCTYPE html>
<html>
    <head>
        <title>step 21</title>
        <style type="text/css">
        #space {background:#dfeeff;height:600px; float: left;}
        #tools {background:#dfffff;height:600px; width: 150px; float: left;}
        #c1, #c2 { width: 100px; }
        </style>

        <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="/js/kinetic-v4.5.4.min.js"></script>
        <script type="text/javascript">
            $('.cnp_item').live('click', function(){
                var item = new Image();
                item.src = $(this).attr('src');
                drawImage(item);
            });

            $('#save').live('click', function(){
                var json = stage.toJSON();
                $('#savespace').val(json);
            });
        </script>


    </head>
    <body>

    <div>
        <div id="tools">
            <img id=c1 class="cnp_item" draggable="true" src="cnp_vectors-vehicles/c1.png">
            <img id=c2 class="cnp_item" draggable="true" src="cnp_vectors-vehicles/c2.png">
            <img draggable="true" src="cnp_vectors-vehicles/impact01.png">
            <img draggable="true" src="cnp_vectors-vehicles/impact02.png">
        </div>
        <div id="space"></div>
    </div>

    <div style="clear: both;"/>
    <button id=save >Store Scene</button>&nbsp;<span id=status></span><br>
    <textarea id=savespace style="width:400px; height: 100px;"></textarea>

    <div style="clear: both;"/>
    <form action=22.php method=post id=restore_form>
        <input type=submit value="Restore Scene"><br>
        <textarea id=fromspace name=fromspace style="width:400px; height: 100px;"><? echo stripslashes($_POST[fromspace]); ?></textarea>
    </form>


    <script defer="defer">

    <? if ($_SERVER[REQUEST_METHOD] == 'POST') { ?>

    var stage = Kinetic.Node.create( $('#fromspace').val() , 'space');

    <? } else { ?>

    var stage = new Kinetic.Stage({
        container: 'space',
        width: 650,
        height: 600
    });

    <? } ?>


    var layer = new Kinetic.Layer();

    function drawImage(imageObj) {

        var cnpImage = new Kinetic.Image({
            image: imageObj,
            x: 0,
            y: -10,
            width: 140,
            height: 140,
            draggable: true
        });

        cnpImage.on('mouseover', function() {
          document.body.style.cursor = 'pointer';
        });
        cnpImage.on('mouseout', function() {
          document.body.style.cursor = 'default';
        });

        layer.add(cnpImage);
        stage.add(layer);
    }



    </script>



    </body>
</html>